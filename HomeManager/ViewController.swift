//
//  ViewController.swift
//  HomeManager
//
//  Created by Kostyantyn Spitsyn on 12/14/14.
//  Copyright (c) 2014 Kostyantyn Spitsyn. All rights reserved.
//

import Foundation
import UIKit

class ViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createTabs()
    }
    
    private func createTabs() {
        var nav1 = UINavigationController()
        var first = FirstViewController(nibName: nil, bundle: nil)
        nav1.viewControllers = [first]
        
        
        var second = SecondViewController(nibName: nil, bundle: nil)
        var nav2 = UINavigationController()
        nav2.viewControllers = [second]
        
        self.viewControllers = [nav1, nav2]
    }
}
