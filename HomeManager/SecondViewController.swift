//
//  SecondViewController.swift
//  HomeManager
//
//  Created by Kostyantyn Spitsyn on 12/14/14.
//  Copyright (c) 2014 Kostyantyn Spitsyn. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    var textField: UITextField?;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.title = "Second"
        // Do any additional setup after loading the view, typically from a nib.
        
        textField = createNewUITextField()
        
        self.view.addSubview(createNewButton())
        self.view.addSubview(textField!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pressed(sender: UIButton!) {
        var alertView = UIAlertView();
        alertView.addButtonWithTitle("Ok");
        alertView.title = "title";
        alertView.message = "Hello, " + textField!.text;
        alertView.show();
    }

    private func createNewButton() -> UIButton {
        let button = UIButton()
        button.setTitle("Hello!", forState: UIControlState.Normal)
        button.backgroundColor = UIColor.blueColor()
        button.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
        button.frame = CGRectMake(60, 80, 50, 20)
        
        button.addTarget(self, action: "pressed:", forControlEvents: UIControlEvents.TouchDown)
        return button
    }
    
    private func createNewUITextField() -> UITextField {
        let textField = UITextField()
        textField.backgroundColor = UIColor.blueColor()
        textField.frame = CGRectMake(60, 140, 100, 20)
        return textField
    }

}

